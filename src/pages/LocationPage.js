import React from 'react'


import '../styles/location.css'

// Import icon button
import editIcon from 'assets/img/other-icon/edit.png';
import settingIcon from 'assets/img/other-icon/setting.png';
import unfreezeIcon from 'assets/img/other-icon/unfreeze.png';

class LocationPage extends React.Component {
    constructor(props) {
        super(props);

        // Dummy data for test purpose only
        const dummy = []
        for (let i = 0; i < 10; i++) {
            dummy.push({
                id: i + 1,
                createTime: '2019-03-01',
                country: 'USA',
                city: 'Washionton',
                plasmaName: `Federal Way, WA ${i + 1}`,
                phone: '253-275-2243',
                status: 'Valid',
            })
        }
        this.state = {
            locations: dummy
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <div className="pull-left">
                    <ul className="nav nav-pills nav-location-tab">
                        <li className="active">
                            <a href="{{ path('location_plasma') }}">Plasma management</a>
                        </li>
                        <li>
                            <a href="{{ path('location_region') }}">Region Group Setting</a>
                        </li>
                        <li>
                            <a href="{{ path('general_setting') }}">Plasma General setting</a>
                        </li>
                    </ul>
                </div>
                <div className="pull-right btn-create-new">
                    <a className="btn btn-blue btn-md">
                        Create
                    </a>
                </div>
                <div className="clearfix"></div>
                <br></br>
                <div className="box plasma">
                    <div className="box-header">
                        <div className="row location-plasma">
                            <table className="table table-location">
                                <tbody>
                                    <tr>
                                        <td className="filter-label">
                                            <label>Filter</label>
                                        </td>
                                        <td>
                                            <select className="form-control input-sm lc-sl-placeholder">
                                                <option value="">Country</option>
                                                <option>Miami</option>
                                                <option>Pembroke Road</option>
                                                <option>Donation Center Name</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select className="form-control input-sm lc-sl-placeholder">
                                                <option value="">State</option>
                                                <option>Miami</option>
                                                <option>Pembroke Road</option>
                                                <option>Donation Center Name</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select className="form-control input-sm lc-sl-placeholder">
                                                <option value="">City</option>
                                                <option>Miami</option>
                                                <option>Pembroke Road</option>
                                                <option>Donation Center Name</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select className="form-control input-sm lc-sl-placeholder">
                                                <option value="">Donation center</option>
                                                <option>Miami</option>
                                                <option>Pembroke Road</option>
                                                <option>Donation Center Name</option>
                                            </select>
                                        </td>
                                        <td className="search-label">
                                            <label>Search</label>
                                        </td>
                                        <td>
                                            <div className="input-group input-group-search input-group-sm">
                                                <input type="text" className="form-control" placeholder="Donation Center Name" />
                                                <span className="input-group-btn">
                                                    <a href="#" role="button" className="btn btn-flat btn-search">
                                                        <i className="fa fa-fw fa-search"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="box-body table-responsive">
                        <table className="table table-plasma-content">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Create Time</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Plasma Name</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th className="h-action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.locations.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{item.id}</td>
                                                <td>{item.createTime}</td>
                                                <td>{item.country}</td>
                                                <td>{item.city}</td>
                                                <td>{item.plasmaName}</td>
                                                <td>{item.phone}</td>
                                                <td>{item.status}</td>
                                                <td className="col-action">
                                                    <a href="#">
                                                        <img src={editIcon} alt="edit" className="icon-edit" />
                                                    </a>
                                                    <a href="{{ path('plasma_setting') }}" target="_blank">
                                                        <img src={settingIcon} alt="setting" className="icon-setting" />
                                                    </a>
                                                    <a

                                                        href="#">
                                                        <img src={unfreezeIcon} alt="unfreeze" className="icon-unfreeze" />
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default LocationPage