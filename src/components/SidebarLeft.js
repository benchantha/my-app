import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

class SidebarLeft extends React.Component {

    render() {
        return (
            <ul className="sidebar-menu" data-widget="tree">
                <li>
                    <Link to="/" className="main-tooltip">
                        <span className="dashboard-icon">
                            <img src={require('../assets/img/menu/dashboard-black.png')} alt="dashboard" />
                        </span>
                        <span className="tooltiptext">Coming soon</span>

                        <span>Dashboard</span>
                    </Link>
                </li>
                <li>
                    <a href="#">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/appointment-black.png')} alt="appointment" />
                        </span>
                        <span>Appointment Management</span>
                    </a>
                </li>
                <li>
                    <a href="{{ path('member_index') }}">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/member-black.png')} alt="member" />
                        </span>
                        <span>Member Management</span>
                    </a>
                </li>
                <li>
                    <a href="{{ path('marketing_index') }}">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/marketing-black.png')} alt="marketing" />
                        </span>
                        <span>Marketing Management</span>
                    </a>
                </li>
                <li>
                    <Link to="/location" className="main-tooltip">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/location-black.png')} alt="location" />
                        </span>
                        <span>Location Setting</span>
                    </Link>
                </li>
                <li>
                    <a href="#" className="main-tooltip">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/reward-black.png')} alt="reward" />
                        </span>
                        <span className="tooltiptext">Coming soon</span>
                        <span>Reward Management</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span className="role-icon">
                            <img src={require('../assets/img/menu/role-black.png')} alt="role" />
                        </span>
                        <span>Role Setting</span>
                    </a>
                </li>
                <li>
                    <a href="#" className="main-tooltip">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/account-black.png')} alt="account" />
                        </span>
                        <span className="tooltiptext">Coming soon</span>
                        <span>Account Management</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/content-black.png')} alt="content" />
                        </span>
                        <span>Content Setting</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/membership-black.png')} alt="membership" />
                        </span>
                        <span>Membership Management</span>
                    </a>
                </li>
                <li>
                    <a href="{{ path('fashcash_index') }}">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/notification-black.png')} alt="fashcash" />
                        </span>
                        <span>Fastcash Record</span>
                    </a>
                </li>
                <li>
                    <a href="#" className="main-tooltip">
                        <span className="menu-icon">
                            <img src={require('../assets/img/menu/fashcash-black.png')} alt="notification" />
                        </span>
                        <span className="tooltiptext">Coming soon</span>
                        <span>Notification Management</span>
                    </a>
                </li>
            </ul>
        );
    }
}

export default SidebarLeft;