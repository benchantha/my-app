import React from 'react';

class Notification extends React.Component {

    render() {
        return (
            <li className="dropdown notifications-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="far fa-bell icon-notification"></i>
                    <span className="label label-danger">10</span>
                </a>
            </li>
        );
    }
}

export default Notification;