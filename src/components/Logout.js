import React from 'react';
import LogoutIcon from '../assets/img/menu/arrow-down.png';

class Logout extends React.Component {

    render() {
        return (
            <li className="dropdown logout-menu">
                <a href="#" className="dropdown-toggle">
                    <img src={LogoutIcon} alt="arrow-down" className="icon-arrow-down"/>
                </a>
            </li>
        );
    }
}

export default Logout;