import React from 'react';
import SidebarLeft from '../SidebarLeft';

class Sidebar extends React.Component {

    render() {
        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <SidebarLeft/>
                </section>
            </aside>
        );
    }
}

export default Sidebar;