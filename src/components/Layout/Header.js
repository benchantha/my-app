import React from 'react';
import Logo from '../Logo';
import Notification from '../Notification';
import UserMenu from '../UserMenu';
import Logout from '../Logout';
import SidebarToggle from '../SidebarToggle';

class Header extends React.Component {

    render() {
        return (
            <header className="main-header">
                <Logo/>
                <nav className="navbar navbar-static-top main-navbar" role="navigation">
                    <div className="navbar-page-title">
                        <ul>
                            <li>
                                <span className="navbar-role">Role:&nbsp;&nbsp;</span>
                                <span className="navbar-managing">State Manager</span>
                            </li>
                            <li>
                                <span className="navbar-role">Managing:&nbsp;&nbsp;</span>
                                <span className="navbar-managing">Florida, United States</span>
                            </li>
                        </ul>
                    </div>
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <Notification/>
                            <UserMenu/>
                            <Logout/>
                            <SidebarToggle/>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;