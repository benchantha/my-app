import { Header, Sidebar, Content } from 'components/Layout';
import React from 'react';

class MainLayout extends React.Component {
 
    render() {
        const { children } = this.props;
        return (
            <div className="hold-transition skin-black fixed sidebar-mini">
                <div className="wrapper">
                    <Header/>
                    <Sidebar/>
                    <Content>
                        {children}
                    </Content>
                </div>
            </div>
        );
    }
}

export default MainLayout;