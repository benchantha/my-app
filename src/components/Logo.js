import React from 'react';
import LogoIcon from '../assets/img/logo.jpeg';

class Logo extends React.Component {

    render() {
        return (
            <a href="#" className="logo">
                <span className="logo-mini"><b>A</b>LI</span>
                <img src={LogoIcon} width="60%"/>
            </a>
        );
    }
}

export default Logo;