import React from 'react';
import UserIcon from '../assets/img/user2-160x160.jpg';

class UserMenu extends React.Component {

    render() {
        return (
            <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <img src={UserIcon} className="user-image" alt="User Image"/>
                    <span className="hidden-xs strong-text">Maria Sanchez</span><br/>
                    <small>EID128318291</small>
                </a>
            </li>
        );
    }
}

export default UserMenu;