import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { MainLayout } from 'components/Layout';
import './styles/main.css';

const DashboardPage = React.lazy(() => import('./pages/DashboardPage'))
const LocationPage = React.lazy(() => import('./pages/LocationPage'))
const AppointmentPages = React.lazy(() => import('pages/appointments/AppointmentPages'));

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <MainLayout>
                        <React.Suspense fallback={() => {
                            return (
                                <span>Loading...</span>
                            )
                        }}>
                            <Route exact path="/" component={AppointmentPages} />
                            <Route exact path="/location" component={LocationPage} />
                        </React.Suspense>
                    </MainLayout>
                    <Redirect to="/" />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
